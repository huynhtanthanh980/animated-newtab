Installation Guide:
   1. Add your video source to script.js according to this form: new videoTab(DISPLAYED_NAME, VIDEO_DIR),
   2. Copy your emergency image (optional) into the folder and rename it to "emergencyImage.png"
   3. Go to extension setting in your browser (edge://extensions/ or chrome://extensions/)
   4. Enable Developer Mode (important) and select "Load Unpacked"

    
Known issues:
Google Chrome will show a warning message whenever you open the browser and this can only be hidden using a very complicated process which is not efficient.
However, Microsoft do not.
