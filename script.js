	class videoTab {
			constructor(name, dir) {
				this.name = name;
				this.dir = dir;
			}
			
			getName() {
				return this.name;
			}
			
			getDir() {
				return this.dir;
			}
		}
	
	//put video directory here
	var videoSource = [
		//put your video here
		//new videoTab(DISPLAYED_NAME, VIDEO_DIR),
	];
	
	var currentVideo = -1;
	var videoPlayer;
	var videoCount = videoSource.length;
	var videoContainer;
	var emImage;
	var button;

	window.onload = function() {
		videoPlayer = document.getElementById('myVideo');
		videoContainer = document.getElementById('container');
		
		changeSource();
		videoPlayer.addEventListener("ended", function(){
			changeSource();
		});
		
		button = document.getElementById('emergencyButton');
		button.addEventListener('click', function() { emergencyClick(); });
		emImage = document.getElementById('emergencyImage');
	}

	function random() {
		return Math.floor(Math.random() * videoCount);
	}

	function changeSource() {
		var nextVideo = random();
		while (nextVideo == currentVideo & videoCount != 1) {
			nextVideo = random();
		}
		currentVideo = nextVideo;
		videoPlayer.src = videoSource[nextVideo].getDir();
		document.title = videoSource[nextVideo].getName();
		videoPlayer.play();
	}

	function emergencyClick() {
		if (videoContainer.style.display === "none") {
			changeSource();
			videoContainer.style.display = "block";
			emImage.style.display = "none";
		} else {
			videoPlayer.pause();
			document.title = "New Tab";
			videoContainer.style.display = "none";
			emImage.style.display = "block";
		}
	}